$(document).ready(function(){
  $('.welcome__slider').slick({
    fade: true,
    autoplay: true,
    autoplaySpeed: 10000,
    prevArrow: $('.welcome__arrow--prev'),
    nextArrow: $('.welcome__arrow--next'),
    speed: 1000,
    dots: true
  });

  $('.tourney-slider').slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 10000,
    prevArrow: '<div class="tourney-slider__arrow tourney-slider__arrow--prev"></div>',
    nextArrow: '<div class="tourney-slider__arrow tourney-slider__arrow--next"></div>',
    responsive: [
      {
        breakpoint: 1200,
        settings: {
          slidesToShow: 2
        }
      },
      {
        breakpoint: 900,
        settings: {
          slidesToShow: 1
        }
      },
      {
        breakpoint: 400,
        settings: {
          slidesToShow: 1,
          arrows: false,
          dots:true
        }
      }
    ]
  });
  //
  $('.header__btn-menu').click(function(e){
    e.preventDefault();

    $(this).toggleClass('header__btn-menu--active');
    $('.header__menu').toggleClass('header__menu--active');
  });

  $('.header__user-avatar-m').click(function () {
    $(this).parents('.header__user').toggleClass('header__user--active');
  })

  $(document).on('click',function(e){
    if(!$(e.target).closest('.header__btn-menu,.header__menu').length) {
      $('.header__btn-menu').removeClass('header__btn-menu--active');
      $('.header__menu').removeClass('header__menu--active');
    }

    if(!$(e.target).closest('.header__user').length) {
      $('.header__user').removeClass('header__user--active');
    }
  });

  // modal
  $('.modal-btn').click(function (e) {
    e.preventDefault();
    var $this = $(this);
    $('.modal').removeClass('modal__in');
    $('.modal').css('top','50%');
    setTimeout(function () {
      // open new
      var scrollTop = $(window).scrollTop(),
        modal = $this.attr('href');
      $(modal).css('top',scrollTop + 100);
      $(modal).addClass('modal__in');
      $('#overlay').fadeIn();
    },300);
  });

  $('.modal__close,#overlay, .modal__btn-close').click(function (e) {
    e.preventDefault();
    $('#overlay').fadeOut();
    $('.modal').removeClass('modal__in')
    $('.modal').css('top','50%');
  });

  //
  var countDownDate = new Date("May 16, 2020 23:37:25").getTime();
  var x = setInterval(function () {
    var now = new Date().getTime();
    var distance = countDownDate - now;
    var days = Math.floor(distance / (1000 * 60 * 60 * 24));
    var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    var seconds = Math.floor((distance % (1000 * 60)) / 1000);
    document.getElementById("timer").innerHTML =  hours + " : "
      + minutes + " : " + seconds;
    if (distance < 0) {
      clearInterval(x);
      document.getElementById("timer").innerHTML = "Время завершено";
    }
  }, 1000);
});


