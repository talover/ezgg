const gulp = require('gulp')

module.exports = function fonts() {
  return gulp.src('src/images/**/*.*')
    .pipe(gulp.dest('build/css/images'))
}


